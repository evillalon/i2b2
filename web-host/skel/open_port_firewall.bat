@echo off
rem -- open port (first argument passed to batch script)
netsh advfirewall firewall add rule name="Open Port %1 in TCP" dir=in action=allow localip=%2 protocol=TCP localport=%1
netsh advfirewall firewall add rule name="Open Port %1 in TCP" dir=out action=allow localip=%2 protocol=TCP localport=%1