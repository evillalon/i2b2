{
	urlProxy: "index.php",
	urlFramework: "js-i2b2/",
	//-------------------------------------------------------------------------------------------
	// THESE ARE ALL THE DOMAINS A USER CAN LOGIN TO
	lstDomains: [
	    { domain: "@I2B2_DOMAIN@",
		  name: "@I2B2_NAME@",
		  project: "@PROJECT_ID@",
		  urlCellPM: "@PM_CELL_URL@",
		  allowAnalysis: true,
		  debug: @DEBUG@
		}, 
		{ domain: "i2b2demo",
		  name: "i2b2demo",
		  urlCellPM: "@PM_CELL_URL@",
		  allowAnalysis: true, 
		  debug: false
		},
		{ domain: "i2b2demo",
		  name: "HarvardDemo",
		  urlCellPM: "http://webservices.i2b2.org/i2b2/services/PMService/",
		  allowAnalysis: true,
		  debug: false
		}
	]
	//-------------------------------------------------------------------------------------------
}
