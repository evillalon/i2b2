# i2b2 installation
###Automates installation of i2b2 on Windows
============================================

####Authors: Elena Villalon

####Contact: https://www.dbmi.columbia.edu/ 

####emails: ev2348@columbia.edu/ evillalon@post.harvard.edu

####Columbia University Department of Biomedical Informatics

It follows very closely the document __i2b2_Installation_Guide.pdf__ and the instructions given in each of the chapters for the installation of the different components of the i2b2 application. 
The main goal of this software is to automate some of the installation tasks in the different chapters of the guide and group them into one single Ant script. Users need to set up some enviromental parameters in each of the _.properties_  files of different folders as described below.  
The Ant scripts of the folders combine several Ant scripts of the i2b2 installation guide and of the software package downloaded from i2b2 websites.   
The installation scripts are written for Windows and SQL Server but it should be easily adaptable to a Linux server and possibly other databases. 
Users need to proceed as follows: 

1. Define the enviromental parameters in *common.properties*; 
2. Download i2b2 software and place it in folder *i2b2-17/*; 
3. Folder *jboss/*, set up *jboss.properties* file and run script *"ant -f  jboss-installation.xml"*. The system enviromental properties may be created with the targets in file *system-variables.xml*. See Chapter 2 of the guide. 
4. Folder *db-sqlserver/*, set up *sqlserver.properties* and invoke the script *"ant -f master_buil.xml"*. See Chapter 3 of the guide; 
5. Folder *web-host/*, set up *webhost.properties* and run script *"ant -f configure-webclient.xml"*. See Chapter 7;
6. Folder *i2b2-install/*, set up *i2b2.properties* and invoke the script *"ant -f  master_build.xml"*. See Chapters 4, 5, 6,9, 10, 11,12, 13

_**Deployment architecture (above):** IIS is a front-end server and contains the i2b2 web client. A PHP script serves as a proxy which allows the web client to invoke JBoss services. The JBoss runtime (with axis 2) hosts the i2b2 cells (pm, crc, ontology, hive, im, fr, work). A SQL Server instance stores the data corresponding to each i2b2 cell._

## Requirements
 * [Ant](http://ant.apache.org/bindownload.cgi)
 * [JDK 7](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
 * [SQL Server 2012](http://www.microsoft.com/en-us/download/confirmation.aspx?id=29062)
 * IIS 7
 * [I2B2 software](https://www.i2b2.org/software/)

### Repository contents
  * __common.properties__ the user needs to set up the enviromental variables that are defined and described in the file.  Note that, because the i2b2 database and JBoss application can be in different servers, 
  the property CONNECTION_URL may need to be set up to the IP address of the DB server when installing the JBoss server. The name of the folder *i2b2-17/* is the property I2B2_FOLDER.   
  
  * __i2b2-17/__ source zip files downloaded from [i2b2 Harvard](https://www.i2b2.org/software/index.html), which contains the i2b2createdb.zip, i2b2core-src-version.zip and i2b2webclient-version.zip. 
	
  * __files-tlp/__ xml and js files that duplicate some of the files in the installation sub-folders */skel* of different directories, i.e. jboss/skel, db-sqlserver/skel, etc.  
	
  * __jboss/__ Folder for the installation of JBOSS. The script downloads jboss from web and configure the ports to listen. Downloads *axis2.war.zip* and configure it inside jboss. After jboss is configured the script *jboss-installation.xml* moves jboss to the location of property JBOSS_HOME. 
  Usage command *"ant -f  jboss-installation.xml"*, which default target (setup-jboss) accomplish the different tasks of the jboss installation and configuration. The user needs to set up the *jboss.properties* file prior of running the script.  Contents of folder:
    * __jboss.properties__ environment variables that the user needs to set up according to the enviroment where jboss, ant and java are installed. 
    * __jboss-installation.xml__ ant script for jboss configuration and installation 
    * __system-variables.xml__ ant script to set environment variables java, ant and jboss homes. Call the script as *"ant -f system-variables.xml set-jboss-home"* for JBOSS_HOME, and same for the others enviromental parameters, i.e. targets *set-java-home* for JAVA_HOME and *set-ant-home* for ANT_HOME. 
	* __jboss-controls.xml__ ant script to start (stop) jboss, i.e. *"ant -f jboss-controls.xml i2b2-start (i2b2-stop)"*.  
	* __skel/__ Folder with files that the *jboss-installation.xml* script needs for the configuration and installation of JBoss. 
    * __work/__ Folder where all downloads are stored and unzipped during installation.
	
  * __db-sqlserver/__ Folder for the configuration of the i2b2 SQLServer database . Users download the compressed i2b2 software package create db and place it in the *i2b2-17/* directory, and set the enviromental parameters in file *sqlserver.properties*. Subsequently, the user invokes the Ant script 
  *"ant -f master_build.xml"*, which default target (setdb) creates the database users and all tables in the i2b2 schema database and also populates them with the Harvard demo data. Contents of folder:
    * __work/__ Folder that the scripts create at run time and  then place the unzipped files of package */i2b2-17/i2b2createdb-1702.zip*.    
	* __skel/__ Folder with '.sql' files that the *master_build.xml* script invokes during installation. The ".sql" files are written for the SQL Server. 
    * __clean_db.xml__ Ant script to start fresh the installation of the database and users. Usage *"ant -f clean_db.xml"*. 
    * __create_db_users.xml__ The *master_build.xml* invokes it to create the databases users.
    * __db_properties.xml__ Also invoke by the *master_build.xml* to configure the  databases users. In addtion, it calculates the IP address of the database server using script *i2b2-db-ip.xml*. 
    * __db_schema.xml__ Ant script that *master_build.xml* uses to create the schema and tables of the i2b2 database and populate them with Harvard Demo data. 
    * __i2b2-db-ip.xml__ Contains a javascript to retrieve the IP address of the Server where database is installed. The property CONNECTION_URL in file *common.properties* is calculated with the javascript code. 
	* __master_build.xml__Ant script that invokes all other scripts in this folder. Usage *"ant -f master_build.xml"*.
	* __sqlserver.properties__The user needs to provide the name of the DB Server and the Admin user and password with enough privilege to execute the installation in databases. 
	
  * __web-host/__ Folder to install the i2b2 webclient.  It depends on the compressed file *i2b2webclient-version.zip* that users put in the directory *i2b2-17/*.  The properties file, *webhost.properties*, needs to be configured for the webclient and php.  Then, users run the Ant script 
  *"ant -f configure-webclient.xml"*, which default target (main) install the webclient with the configuration files and moves it to the location specified in *common.properties*. Contentents of folder:
     * __work/__ Folder that the scripts use to place the unzipped files while installing the webclient.  
     * __skel/__ Holds some of the configuration files that are modified during installation.
     * __configure-webclient.xml__ Ant script to configure and install webclient, it uses the files in subdirectory *skel/*. Usage *"ant -f configure-webclient.xml"*.
     * __host-conf.xml__ Ant scripts with targets to download IIS and configure some of the ports. It has not been tested thorougly.  
	 * __i2b2-ip.xml__ Contains javascript to obtain the IP address of the host, use during installation.
	 * __installation-php.xml__ Ant script to download and configure PHP for IIS Server. Usage *"ant -f installation-php.xml"*.
	 * __webhost.properties__ Properties file that users need to set up with  their enviromental parameters. 
	 
  * __i2b2-install/__ Folder for the configuratin of the JBoss server. Prior to run the *master_build.xml script*, users need first to install JBoss with the command *"ant -f  jboss-installation.xml"* within the *jboss/* folder. Users also need to install and configure the i2b2 database 
  running target *"ant -f master_build.xml"* within the folder *db-sqlserver/*.  Then, download the i2b2 software package, *i2b2core-src-version.zip*, and place the zip folder in the *i2b2-17/* directory. Finally, run the script *master_build.xml* in this folder, which default target (install-all) call the different Ant scripts for the jboss configuration. Contents of folder:
    * __work/__ Holds the i2b2 core package that the *master_build.xml* unzips within this folder.
	* __skel/__ Holds some of the configuration files use during installation and  configuration of the different cells as described next. 
	* __admin-install.xml__ To install the Admin module in the IIS Server. Usage *"ant -f admin-install.xml"*, which default target is install-admin.  
	* __common-install.xml__ Contain the scripts for server-common cell that invokes the build.xml of the i2b2 software package.   
	* __crc-install.xml__ To install and configure CRC cell in jboss server that invokes the master_build.xml of i2b2 software package. 
    * __execute-build.xml__ Macrodef use in conjuction with the installations of the cells. 
	* __fr-build.xml__ To install FR cell that invokes the master_build.xml of the i2b2 software and the configuration files stored in *skel/*. 
    * __i2b2.properties__ Property file that users need to setup according to the version of i2b2 software and admin user/password in the database.  
    * __i2b2-ip.xml__ The javascript finds the ip address of the jboss server. 
    * __im-install.xml__ To install IM cell that invokes the master_build.xml of the i2b2 software and configuration files stored in *skel/*.
	* __master_build.xml__ The ant script which default target calls all the xml files in this folder and accomplish the installation of software in package *i2b2core-src-version*. 
    * __ont-install.xml__ Part of the installation, it configures the ONT cell in jboss. Invoke master_build.xml of the ontology cell in the i2b2 core package and uses the file in *skel/* for configuration.
	* __pm-install.xml__ For installation of PM cell. Invoke master_build.xml of the core software for PM cell.
	* __work-install.xml__ For installation of Workplace. Invoke master_build.xml of the core software for Work cell.
	